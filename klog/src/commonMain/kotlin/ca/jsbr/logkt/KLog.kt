@file:Suppress("unused")

package ca.jsbr.logkt

import ca.jsbr.logkt.middleware.PrettyPrint
import ca.jsbr.logkt.transporters.NativeConsoleTransporter

var klog = KLog()
    .apply {
        add(PrettyPrint())
        add(NativeConsoleTransporter())
    }


class KLog(private val default: Map<String, String> = LinkedHashMap()) : Logger {

    var level: LogLevel = DefaultLevel.DEBUG

    val logItems = arrayListOf<LogItem>()

    override fun add(index: Int,vararg logItem: LogItem) {
        logItems.addAll(index, logItem.toList())
    }

    override fun add(vararg logItem: LogItem) {
        logItems.addAll(logItem)
    }

    inline fun <reified T : LogItem> getItem(): T = logItems.first { it is T } as T

    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {
        if (this.level.value > level.value) return messages
        default.forEach { messages[it.key] = it.value }
        val result: MutableMap<String, String>? = messages
        logItems.fold(result) { msg, logItem -> if (msg != null) logItem.log(level, msg) else null }
        return messages
    }
}


fun Logger.child(vararg pair: Pair<String, String> = arrayOf()) =
    KLog(pair.toMap()).apply { add(this@child) }

fun Logger.child(context: String): Logger =
    klog.child("context" to context).apply { add(this@child) }

fun Logger.debug(message: String, vararg pair: Pair<String, String>) = log(DefaultLevel.DEBUG, message, *pair)
fun Logger.info(message: String, vararg pair: Pair<String, String>) = log(DefaultLevel.INFO, message, *pair)
fun Logger.warn(message: String, vararg pair: Pair<String, String>) = log(DefaultLevel.WARN, message, *pair)
fun Logger.error(message: String, vararg pair: Pair<String, String>) = log(DefaultLevel.ERROR, message, *pair)

fun Logger.log(level: LogLevel, message: String) {
    val map = linkedMapOf("level" to level.label, "message" to message)
    log(level, map)
}

fun Logger.log(level: LogLevel, message: String, vararg pair: Pair<String, String>) {
    val map = linkedMapOf("level" to level.label, "message" to message, *pair)
    log(level, map)
}

