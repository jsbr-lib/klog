package ca.jsbr.logkt.Utils

enum class ConsoleColors(val unix: String) {

    CLEAR("\u001B[0m"),
    DEFAULT(""),
    DEFAULT_BOLD(""),
    DEFAULT_BACKGROUND(""),

    BLACK("\u001b[0;30m"),
    RED("\u001b[0;31m"),
    GREEN("\u001b[0;32m"),
    YELLOW("\u001b[0;33m"),
    BLUE("\u001b[0;34m"),
    PURPLE("\u001b[0;35m"),
    CYAN("\u001b[0;36m"),
    WHITE("\u001b[0;37m"),

    BLACK_BOLD("\u001b[1;30m"),
    RED_BOLD("\u001b[1;31m"),
    GREEN_BOLD("\u001b[1;32m"),
    YELLOW_BOLD("\u001b[1;33m"),
    BLUE_BOLD("\u001b[1;34m"),
    PURPLE_BOLD("\u001b[1;35m"),
    CYAN_BOLD("\u001b[1;36m"),
    WHITE_BOLD("\u001b[1;37m"),

    BLACK_BACKGROUND("\u001b[40m"),
    RED_BACKGROUND("\u001b[41m"),
    GREEN_BACKGROUND("\u001b[42m"),
    YELLOW_BACKGROUND("\u001b[43m"),
    BLUE_BACKGROUND("\u001b[44m"),
    PURPLE_BACKGROUND("\u001b[45m"),
    CYAN_BACKGROUND("\u001b[46m"),
    WHITE_BACKGROUND("\u001b[47m"),
}