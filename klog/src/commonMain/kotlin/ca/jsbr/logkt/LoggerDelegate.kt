@file:Suppress("unused")

package ca.jsbr.logkt

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

class LoggerDelegate(private val parent: Logger = klog, private vararg val pair: Pair<String, String> = arrayOf()) : ReadOnlyProperty<Any, Logger> {
    private var logger: Logger? = null
    override operator fun getValue(thisRef: Any, property: KProperty<*>): Logger {
        var name = thisRef.className()
        name = name.split(".").takeLast(4).joinToString(".")
        logger = logger ?: parent.child(
            "context" to name,
            *pair)
        return logger!!
    }
}

fun Any.className() = this::class.toString().split(" ")[1]
fun KClass<*>.className() = this.toString().split(" ")[1]
inline fun <reified T> nameOf() = T::class.toString().split(" ")[1]

fun Logger.context(vararg pair: Pair<String, String> = arrayOf()): LoggerDelegate =
    LoggerDelegate(this, *pair)

