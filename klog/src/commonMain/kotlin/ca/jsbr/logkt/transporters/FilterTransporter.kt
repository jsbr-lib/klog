package ca.jsbr.logkt.transporters

import ca.jsbr.logkt.LogLevel

class FilterTransporter(private val filter: (level: LogLevel, MutableMap<String, String>) -> Boolean) : Transporter {

    constructor(key: String, map: Map<String, LogLevel>) : this({ level, msg ->
        level >= map[msg[key]]
    })

    constructor(key: String, vararg item: Pair<String, LogLevel>) : this(key, item.toMap())

    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String>? {
        if (filter(level, messages))
            return messages
        return null
    }
}