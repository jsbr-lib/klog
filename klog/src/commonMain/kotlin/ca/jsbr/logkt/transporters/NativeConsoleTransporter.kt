package ca.jsbr.logkt.transporters

import ca.jsbr.logkt.ColorStyle
import ca.jsbr.logkt.DefaultLevel
import ca.jsbr.logkt.LogLevel

expect class NativeConsoleTransporter(): Transporter {
    var styles:Map<LogLevel, ColorStyle>
    var enableColor:Boolean
}
