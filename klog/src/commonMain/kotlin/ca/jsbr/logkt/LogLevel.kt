package ca.jsbr.logkt

interface LogLevel {
    operator fun compareTo(other: LogLevel?): Int = compareTo(other?.value ?: -99999f)
    operator fun compareTo(other: Float): Int = if (value < other) -1 else if (value > other) 1 else 0

    val value: Float
    val label: String
    val mappedLevel: DefaultLevel
}