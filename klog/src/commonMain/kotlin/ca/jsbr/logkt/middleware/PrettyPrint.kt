package ca.jsbr.logkt.middleware

import ca.jsbr.logkt.LogLevel

class PrettyPrint(var formater: (level: LogLevel, messages: MutableMap<String, String>) -> String = ::basicFormatter) : Middleware {
    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {
        var msg = formater(level, messages)
        msg += messages.entries
            .filter { it.key != "level" && it.key != "message" }
            .joinToString { "\n\t ${it.key}: ${it.value}" }
        messages["message"] = msg
        return messages
    }
}

fun basicFormatter(level: LogLevel, messages: MutableMap<String, String>): String {

    val result = arrayListOf("[${level.label.padEnd(5, ' ')}]")

    if ("context" in messages)
        result.add("${messages.remove("context")} -")

    result.add(messages["message"]!!)

    return result.joinToString(" ")
}