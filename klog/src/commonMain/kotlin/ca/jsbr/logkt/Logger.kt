package ca.jsbr.logkt

interface Logger : LogItem {
    fun add(vararg logItem: LogItem)
    fun add(index: Int, vararg logItem: LogItem)
}

