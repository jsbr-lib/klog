package ca.jsbr.logkt.transporters

import android.util.Log
import ca.jsbr.logkt.Color
import ca.jsbr.logkt.ColorStyle
import ca.jsbr.logkt.DefaultLevel
import ca.jsbr.logkt.LogLevel
import ca.jsbr.logkt.Utils.ConsoleColors

actual class NativeConsoleTransporter : Transporter {

    private var lastIsError = false

    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {
        val msg = messages["message"]
        val tag = messages["tag"] ?: "klog"
        when{
            level <= 0f -> Log.v(tag,msg)
            level <= DefaultLevel.DEBUG -> Log.d(tag,msg)
            level <= DefaultLevel.INFO -> Log.i(tag,msg)
            level <= DefaultLevel.WARN -> Log.w(tag,msg)
            level <= DefaultLevel.ERROR -> Log.e(tag,msg)
            else -> Log.wtf(tag,msg)
        }
        return messages
    }


    actual var styles: Map<LogLevel, ColorStyle> = Color.defaultColor
    actual var enableColor: Boolean = true
}


