package ca.jsbr.logkt.transporters

import ca.jsbr.logkt.Color
import ca.jsbr.logkt.ColorStyle
import ca.jsbr.logkt.LogLevel
import ca.jsbr.logkt.Utils.ConsoleColors

actual class NativeConsoleTransporter : Transporter {

    private var lastIsError = false

    override fun log(level: LogLevel, messages: MutableMap<String, String>): MutableMap<String, String> {
        var msg = messages["message"]
        if (enableColor)
            msg = getStyle(level) + msg + ConsoleColors.DEFAULT.unix + ConsoleColors.WHITE.unix
        println(msg)
        return messages
    }

    private fun getStyle(level: LogLevel): String {
        val style = styles[level] ?: styles[level.mappedLevel] ?: return ""
        var result = ConsoleColors.CLEAR.unix
        result += if (style.bold) ConsoleColors.valueOf(style.color.name + "_BOLD").unix else ConsoleColors.valueOf(style.color.name).unix
        result += ConsoleColors.valueOf(style.background.name + "_BACKGROUND").unix
        return result
    }

    actual var styles: Map<LogLevel, ColorStyle> = Color.defaultColor
    actual var enableColor: Boolean = true
}


